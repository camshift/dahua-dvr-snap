from sys import argv
import os

os.chdir(os.path.dirname(__file__))
filepath = os.path.basename("".join(argv[1:]))
filename = os.path.splitext(filepath)[0]
params = filename.split('_')
ip, _, user, password, ch = params
os.system('tanidvr -m1 -t %s -u %s -w %s -c %s | mplayer -vf screenshot -cache 32 - 2>/dev/null' % (ip, user, password, str(int(ch) - 1)))
