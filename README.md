## Dahua DVR snap

Helper for streams watch: open videoplayer by screenshot with naming 1.2.3.4_37777_admin_admin_7.jpg (Dahua bruter++ format)

Source of deb-package, contains x64-compiled tanidvr and dhav2mkv.

*Using:* mouse menu -> Open with -> DVR Snap

*Requirements:* mplayer.

*Resourses:*
- bruter: [version 1](https://t.me/camshift/7), [version 2](https://t.me/camshift/13)
- package: [deb](https://t.me/camshift/25)